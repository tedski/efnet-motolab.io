Title: documents
Slug: docs

Some documents you may find handy:

| Document Name | Description |
| ------------- | ----------- |
| [Bill of Sale] | A generic Bill of Sale that can be used for any motorcycle purchase. |
| [MSF Parking Lot Exercises] | Handy parking lot exercises from the MSF. |
| [Fault Finding Diagram] | A flow chart to diagnose electrical and charging system issues on most any bike. Mulit-meter and charged battery required. |
| [Gearing spreadsheet] | A spreadsheet to easily experiment with gearing ratios. |
| [Suspension] | An article from Superbike magazine regarding suspension. |
| [How to ride pillion] | An article from one of our own, regarding how to be a passenger on a motorcycle. |

[bill of sale]: http://static.efnetmoto.com/docs/Bill_of_Sale.doc
[MSF Parking Lot Exercises]: http://static.efnetmoto.com/docs/MSF_ParkingLotExercises.pdf
[fault finding diagram]: http://static.efnetmoto.com/docs/fault-finding-diagram.pdf
[gearing spreadsheet]: http://static.efnetmoto.com/docs/gearing.xls
[suspension]: http://static.efnetmoto.com/docs/suspension.pdf
[How to ride pillion]: http://www.chaosreigns.com/pillion/